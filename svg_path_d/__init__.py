# -*- encoding: utf-8 -*-


from importlib.metadata import version as _version

from . import basic, raster


__author__ = 'Umesh Mohan <moh@nume.sh>'
__copyright__ = '2021, Umesh Mohan'

__version__ = _version('svg_path_d')
